package javkajavka;

import java.util.ArrayList;
import java.util.List;

public class ChartSettings {
    List<ChartSerie> series = new ArrayList<>();
    String title;
    String subtitle;
    boolean haveLegend;
    ChartType chartType;
}
