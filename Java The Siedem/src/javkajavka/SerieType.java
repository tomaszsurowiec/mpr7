package javkajavka;

public enum SerieType {
    Line,
    Point,
    LinePoint,
    Bar,
    Area
}
